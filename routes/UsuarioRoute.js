const express = require('express')
const router = express.Router()
const { UsuarioController } = require('../controllers')

router.post('/usuario/login', UsuarioController.login)

module.exports = router;