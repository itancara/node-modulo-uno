const express = require('express')
const router = express.Router()

const { ArchivoController } = require('../controllers')

router.post('/archivo/escribir', ArchivoController.escribirArchivo)

router.put('/archivo/actualizar/:nombreArchivo', ArchivoController.actualizarArchivo)

router.get('/archivo/leer/:nombreArchivo', ArchivoController.leerArchivo)


module.exports = router