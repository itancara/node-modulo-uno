const express = require('express')
const router = express.Router()
const { PersonaController } = require('../controllers')
const { AuthMiddleware, PermisoMiddleware } = require('../middlewares')

router.get('/persona',
  AuthMiddleware.verificarToken,
  PermisoMiddleware.verificarPermiso('persona:listar'),
  PersonaController.listarPersonas)

module.exports = router
