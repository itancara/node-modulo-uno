const express = require('express')
const router = express.Router()

const { RenderController } = require('../controllers')

router.get('/render/html', RenderController.renderHTML)

module.exports = router