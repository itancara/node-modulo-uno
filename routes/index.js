module.exports = {
  UsuarioRoute: require('./UsuarioRoute'),
  PersonaRoute: require('./PersonaRoute'),
  ArchivoRoute: require('./ArchivoRoute'),
  RenderRoute: require('./RenderRoute')
}