const jwt = require('jsonwebtoken')
const secret = 'KEY_CODE'

const verificarToken = (req, res, next) => {
  try {
    const { authorization } = req.headers
    const decodificado = jwt.verify(authorization, secret)
    console.log('QUERY MIDDLEWARE === ', req.query)
    req.query.nombreCompleto = 'Ivan Tancara'

    next()
  } catch (error) {
    res.status(400).json({ mensaje: error.message })
  }
}

module.exports = {
  verificarToken
}