const verificarPermisos = (req, res, next) => {
  console.log('SE EJECUTA PERMISO MIDDLWARE.')
  next()
}

const verificarPermiso = (permiso) => {
  return (req, res, next) => {
    console.log(permiso)
    next()
  }
}

module.exports = {
  verificarPermisos,
  verificarPermiso
}