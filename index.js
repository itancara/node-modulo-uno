const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const port = 3000;

app.use(bodyParser.json())
// GET, POST, PUT, DELETE, PATCH
// FRONTED
// 1. localhost:3000/personas?nombre=Ivan  ->  Query Params -> req.query
// 2. localhost:3000/personas/120          ->  Params       -> req.params 
// 3. localhost:3000/personas              ->  body         -> req.body
// { nombres: 'Ivan' }

let id = 1;
const personas = [
  {
    id: id,
    nombres: 'Dilian',
    apellidoPaterno: 'Calle',
    apellidoMaterno: 'Cruz'
  }
];

// GET (LISTAR, MOSTRAR, REPORTE)
app.get('/personas', (req, res) => {
  // res.send('HOLA DESDE NODE JS')
  // const mensaje = { saludo: 'HOLA DESDE NODE JS dsadsas' }
  res.status(200).json(personas);
});

function generarId () {
  id = id + 1;
  return id;
}
// POST (CREAR)
app.post('/personas', (req, res) => {
  // body
  try {
    const datos = req.body;
    if (datos.nombres === 'Homero') {
      throw new Error('No se permiten Homeros')
    }
    datos.id = generarId();
    personas.push(datos)
    res.status(201).json(datos);
  } catch (error) {
    res.status(412).json({
      mensaje: error.message
    })
  }
});

// PUT (ACTUALIZAR MAS DE UN ATRIBUTO DE UN OBJETO)
app.put('/personas/:idPersona', (req, res) => {
  const id = req.params.idPersona;
  const datos = req.body;
  for (const persona of personas) {
    if (persona.id.toString() === id.toString()) {
      persona.nombres = datos.nombres;
      persona.apellidoPaterno = datos.apellidoPaterno;
      persona.apellidoMaterno = datos.apellidoMaterno;
    }
  }
  res.status(200).json(personas)
});

// CREAR LA RUTA PARA ELIMINAR UNA PERSONA DEL ARRAY DE PERSONAS
// DELETE (ELIMINAR UN OBJETO DE LA BD)
app.delete('/personas/:idPersona', (req, res) => {
  const id = req.params.idPersona;
  const posicion = personas.findIndex((x) => x.id == id);
  if (posicion == -1) {
    return res.status(400).json({ mensaje: 'El id de la persona no existe' })
  }
  personas.splice(posicion, 1)
  res.status(200).json({ mensaje: 'Eliminado correctamente.' })
});

// PATCH (ACTUALIZAR UN SOLO ATRIBUTO) 
app.patch('/personas/:idPersona/cambiar-apellido-paterno', (req, res) => {
  const id = req.params.idPersona;
  const apellidoPaterno = req.body.apellidoPaterno;
  for (const persona of personas) {
    console.log(persona.id, id)
    if (persona.id == id) {
      persona.apellidoPaterno = apellidoPaterno
    }
  }
  res.status(200).json({ mensaje: 'Actualizado correctamente.' })
});


app.listen(port, () => {
  console.log('Funcionando en el puerto ' + port)
});
