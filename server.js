const express = require('express');
const bodyParser = require('body-parser');
const { query } = require('express');
const app = express();
const port = 3000;
app.use(bodyParser.json())

const { UsuarioRoute, PersonaRoute, ArchivoRoute, RenderRoute } = require('./routes')

app.use(UsuarioRoute)
app.use(PersonaRoute)
app.use(ArchivoRoute)
app.use(RenderRoute)

app.listen(port, () => {
  console.log('Funcionando en el puerto ' + port)
});

// PRACTICA
// GENERAR RUTA PARA LISTAR DEPARTAMENTOS 
// (PROTEGIDA POR VERIFICACION DE TOKEN)
// GENERAR CONTROLADOR PARA LISTAR DEPARTAMENTOS