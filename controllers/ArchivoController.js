const fs = require('fs')

const escribirArchivo = (req, res) => {
  const { contenido, nombreArchivo } = req.body
  const dir = 'archivos-escritos';
  if (!fs.existsSync(dir)) {
    fs.mkdirSync('archivos-escritos')
  }
  
  fs.writeFileSync(`archivos-escritos/${nombreArchivo}.txt`, JSON.stringify(contenido))
  res.status(200).json({ mensaje: 'Archivo escrito correctamente' })
}

const actualizarArchivo = (req, res) => {
  const { nombreArchivo } = req.params;
  const { contenido } = req.body;

  if (!fs.existsSync(`archivos-escritos/${nombreArchivo}.txt`)) {
    res.status(400).json({ mensaje: `El archivo ${nombreArchivo}, no existe.` })
  }
  let contenidoFinal = fs.readFileSync(`archivos-escritos/${nombreArchivo}.txt`, { encoding: 'utf-8' })
  contenidoFinal += contenido;
  fs.writeFileSync(`archivos-escritos/${nombreArchivo}.txt`, contenidoFinal)
  res.status(200).json({ mensaje: 'Archivo actualizado correctamente.' })
}

const leerArchivo = (req, res) => {
  const { nombreArchivo } = req.params;
  if (!fs.existsSync(`archivos-escritos/${nombreArchivo}.txt`)) {
    res.status(400).json({ mensaje: `El archivo ${nombreArchivo}, no existe.` })
  }
  const contenidoArchivo = fs.readFileSync(`archivos-escritos/${nombreArchivo}.txt`, { encoding: 'utf-8' })
  res.status(200).json({ mensaje: JSON.parse(contenidoArchivo) })
}

module.exports = {
  escribirArchivo,
  leerArchivo,
  actualizarArchivo
}