const jwt = require('jsonwebtoken')
const secret = 'KEY_CODE'

const login = (req, res) => {
  try {
    const { usuario, contrasena } = req.body;
  
    if (usuario !== 'admin') {
      throw new Error('Error en su usuario.')
    }
  
    if (contrasena !== 'admin') {
      throw new Error('Error en su contraseña.')
    }
    
    const token = jwt.sign({
      idRol: 1,
      idEntidad: 1,
      usuario
    }, secret, { expiresIn: '1h' })

    res.status(200).json({ token })
  } catch (error) {
    res.status(400).json({ mensaje: error.message })    
  }

}

module.exports = {
  login
}