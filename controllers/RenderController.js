const ejs = require('ejs')
const pdf = require('html-pdf')

const renderHTML = async (req, res) => {
  const renderizado = await ejs.renderFile('controllers/plantilla.ejs', { nombreCompleto: 'Ivan Tancara' })
  const options = { format: 'Letter' };
  // pdf.create(renderizado, options).toFile('nombreCompleto.pdf', (err, res) => {
  //   if (err) return console.log(err);
  //   console.log(res);
  // })

  await pdf.create(renderizado, options).toBuffer((err, respuesta) => {
    if (err) return console.log(err);
    res.send(respuesta.toString('base64'));
  })
}

module.exports = {
  renderHTML
}

// Mostrar nombre, apellidoPaterno, apellidoMaterno, celular, en una vista HTML.
// Los datos deben ser pasados por body, desde la peticion.