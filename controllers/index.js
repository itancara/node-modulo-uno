module.exports = {
  UsuarioController: require('./UsuarioController'),
  PersonaController: require('./PersonaController'),
  ArchivoController: require('./ArchivoController'),
  RenderController: require('./RenderController')
}